# Convertisseur csv : SaCoche => Moodle

Script python3, requiert uniquement la bibliothèque standard

## Usage 

**En ligne de commande**

```bash
./convert_csv_educ_nat -h
usage: convert_csv_educ_nat_StdLibrary_version [-h] -i FILE_IN [-w FILE_WANTED]
                                               [-o FILE_OUT]

Convertisseur csv format Moodle vers format SacoCoche (Version bibliothèque
standard)

optional arguments:
  -h, --help            show this help message and exit
  -i FILE_IN, --input FILE_IN
                        csv à convertir
  -w FILE_WANTED, --wanted FILE_WANTED
                        csv avec les entêtes souhaitées (optionel)
  -o FILE_OUT, --ouput FILE_OUT
                        csv converti à inscrire (optionel)
```

```bash
./convert_csv_educ_nat -i fichier_a_convertir.csv
```

**Dans un script python3**
```python
>>> from csv_converter import convert_csv
>>> convert_csv('fichier_a_convertir.csv')
```

## Règles de conversion (& autres paramètres):

Ajoutez des règles de conversion dans le fichier "csv_converter/convertions_definitions.py"

#### Contenu du fichier:

Les transformations à effectuer sont sous forme d'une [liste] de {dictionnaires}.
La liste est nommée *CONVERSIONS*. Les dictionnaires ont chacuns les entrées suivantes:

| Entrée | Type | Description |
| ----- | ------- | - |
| **"in"**| *str* ou *list of str* | le nom (ou une liste de noms) de colonne(s) à transformer  |
| **"out"**  | *str*   | le nom de la colonne du champs à inscrire  |
| **"func"** | *function*     | une fonction qui effectue les transformations |

Lors d'un changement de niveau, thème, domaine, une ligne est insérée selon les règles
décrites dans la liste [CATEGORY_CHANGE].
Cette liste contient des {dictionnaires} aux entrées suivantes:

| Entrée | Type | Description |
| ----- | ------- | - |
| **conversions** | convertion definitions | une structure identique à "*CONVERSIONS*" |
| **check** | string | nom de la colonne à surveiller
| **prev_val** | variable | fixée à None, prend tour à tour la valeur de la colonne 'check' de la ligne traitée précédente | 

#### Spécifications de la fonction:

Une fonction peut prendre plusieurs champs en entrée, mais ne peut remplir qu'un champ à la fois.
Généralement, une connaissance basique des expressions régulières est suffisante à leur construction.

|  | Type | Description |
| --- | ------- | - |
| **arguments**   | \**str (unpacked list)*  | le(s) texte(s) d'une ligne contenue dans la(es) colonne(s) définies par 'in'  |
| **retour**      | *str*     | le nouveau champ à inscrire     |


#### Exemples:

*Fonctions*:
	
```python
def get_ID(text):
    return re.search("C\d+(.\d+)*", text)[0].replace(".", "")

def fonction_de_conversion(*texts):
    """ multiple colonnes en entrée """

    extrait_1 = texts[0]
    extrait_2 = texts[1]
    
    return f'... {extrait_1} ... {extrait_2} ...'
```

*Conversions*:

```python
CONVERSIONS = [
    {
        "in": "ITEM",  # colonne  où se trouvent les infos
        "out": "ID",  # colonne à remplir
        "func": get_ID  # la fonction pour extraire le texte souhaité
    },
    {
        "in": ["COMMENTAIRE", "ITEM"],
        "out": "Format de description",
        "func": fonction_de_conversion
    },
    {
        "in": "COMMENTAIRE",
        "out": "Description",
        "func": lambda text: f"<p>{text}</p>"
    }
]
```

## Paramètres

Certains paramètres sont contenus et modifiables dans le fichiers "csv_converter/parmeters.py", les voici:

| Variable | Type | Description |
| ----- | ------- | - |
| **DEFAULT_HEADERS**      | list of str   | les entêtes par défaut   |
| **DEFAULT_OUT_FILE**    | str   | le fichier de sortie par défaut  |
| **CONVERSIONS**     | list of dict     | la définition des transformations nécessaires à la conversion du fichier csv |
| **IN_CSV_DELIMITER** | a character | séparateur du fichier d'entrée |
| **IN_CSV_QUOTECHAR** | a character | symbole de citation (texte) du fichier d'entrée |
| **OUT_CSV_DELIMITER** | a character | séparateur du fichier de sortie |
| **OUT_CSV_QUOTECHAR** | a character | symbole de citation (texte) du fichier de sortie |
| **VALEURS_DU_BAREME** / **FORMAT_DE_DESCRIPTION** / **CONFIGURATION_DU_BAREME** / **TAXONOMIE** | str | champs fixes |
| **CXY_SEPs** | list of character | séparateur dans les références : C1.2.3 => sep = "." |

