#!/usr/bin/env python3

"""

# Définitions des conversions

=============================

## Intro
========

Ce fichier contient la définition des transformation nécessaires à la conversion du fichier csv

------------------------------------------------------------------------

## Structure
============

Par tranformation, il est entendu une action, efféctuée ligne par ligne, de type:

> case(s) en entrée --> modification --> case de sortie

Chaque transformation est alors définie par trois paramètres.
Ils seront spécifiés dans un dictionnaire. Soit les entrées de ce dictionnaire:

    "in" (str) or (list of str) : le nom (ou une liste de noms) de colonne(s) à transformer
    "out" (str): le nom de la colonne du champs à inscrire
    "func" : une fonction qui effectue les transformations

Enfin, une liste contiendra l'ensemble des conversions (dictionnaires) à éffectuer sur une seule et même ligne.
Cette liste est nommée **CONVERSIONS**.

Cependant, il faut aussi introduire de nouvelles lignes notifiant un changement de catégorie (thème, domaine...)
Au fur et à mesure du parcourt des lignes du fichier, le contenu d'une colonne spécifiée est surveillé, si il y a changement,
une nouvelle ligne est introduite
Soit cette nouvelle structure, un dictionnaire, de contenu:

    'conversions' : la liste de conversions (selon la structure précédente),
    'prev_val' : une variable pour retenir la catégorie traitée (fixé à None au départ)
    'check' : le nom de la colonne à surveiller, si celle ci est diffère de prev_val, une ligne est introduite

De même une liste contient l'ensemble de ces définitions.
Cette liste est nommée **CATEGORY_CHANGE**

------------------------------------------------------------------------

## Format de la fonction
========================

fonction_de_conversion:

    arguments :
        text (str) : le champ de la colonne "in" si "in" est le nom d'une unique colonne
             (unpacked list of str) : une suite de champs si "in" est une liste de noms de colonne
    retourne :
        (str) : un texte, le nouveau champ à inscrire

------------------------------------------------------------------------

## Schéma de la structure
=========================

Pour les nouvelles catégories:
```
CATEGORY_CHANGE =
    [
        {
            'conversions':
                [
                    {
                        'in': "colonne_i",
                        'out': "colonne_j",
                        'func': ma_fonction
                    },
                    ...
                ]
            'prev_val' : None
            'check' : "colonne_k"
        },
        ...
    ]
```

Pour les conversions de lignes:
```
CONVERSIONS =
    [
        {
            'in': "colonne_i",
            'out': "colonne_j",
            'func': ma_fonction
        },
        ...
    ]
```

------------------------------------------------------------------------

## Exemples
===========

Soit par exemple

```
    def get_parent_ID(text):
        text = text.replace('-','.')
        return re.search("C\d+(.\d+)*", text)[0]#.rpartition(".")[0]#.replace(".", "")

    CONVERSIONS = [
        {
            "in": "ITEM",
            "out": "ID de la compétence parente",
            "func": get_parent_ID
        },
        {
            "in": ["COMMENTAIRE", "ITEM"],
            "out": "Format de description",
            "func": multi_colomn_conv
        },
    ]
```

## Créer ses propres conversions
=================================

Ajoutez simplement vos entrées dans les dictionnaires
Pour construire une fonction de conversion,
une connaissance sommaire du fonctionnement des expressions régulières est suffisante.

"""

from .parameters import (
    VALEURS_DU_BAREME,
    FORMAT_DE_DESCRIPTION,
    CONFIGURATION_DU_BAREME,
    TAXONOMIE,
    CXY_SEPs
)

# module pour regex
import re

###################################################################
################## Chagement de catégorie #########################
###################################################################

def better_join(*ll, sep=' '):
    text=''
    for l in ll:
        if isinstance(l, str):
            text += l + sep
        else:
            text += sep.join(l)+sep
    return text[:-len(sep)]

def new_niveau_nom(text):
    text = text.split(' ')
    diplome = text[0][1:-1]
    intitule = [mot.title() for mot in text[1:]]
    nom = better_join(diplome, intitule)

    return nom

def new_niveau_id(text):
    text = text.split(' ')
    diplome = text[0][1:-1]
    accronyme = ''.join([mot[0].upper() for mot in text[1:] if len(text)!=0])
    ID = better_join(diplome, accronyme)

    return ID

NEW_NIVEAU = [
    {
        'in' : 'NIVEAU',
        'out' : 'Nom abrégé',
        'func' : new_niveau_nom
    },
    {
        'in' : 'NIVEAU',
        'out' : 'ID',
        'func' : new_niveau_id
    },
    {
        'in': 'NIVEAU', # non utilisé
        'out': 'Valeurs du barème',
        'func' : lambda x : VALEURS_DU_BAREME
    },
    {
        'in': 'NIVEAU', # non utilisé
        'out': 'configuration du barème',
        'func' : lambda x : CONFIGURATION_DU_BAREME
    },
    {
        'in': 'NIVEAU', # non utilisé
        'out': 'Taxonomie',
        'func' : lambda x : TAXONOMIE
    },
    {
        'in' : 'NIVEAU', # non utilisé
        'out' : 'Format de description',
        'func' : lambda x : FORMAT_DE_DESCRIPTION
    },
    {
        'in' : 'NIVEAU', # non utilisé
        'out' : 'Est un référentiel',
        'func' : lambda x : 1
    },
    {
        'in' : 'NIVEAU', # non utilisé
        'out' : 'Format de description',
        'func' : lambda x : FORMAT_DE_DESCRIPTION
    },
]

def get_domaine_id(text):
    text = re.sub('|'.join(CXY_SEPs), '.', text) # remplace les séparateurs par des .
    return text.split(' ')[0]

NEW_DOMAINE = [
    {
        'in' : 'DOMAINE',
        'out' : 'Nom abrégé',
        'func' : lambda text : text
    },
    {
        'in' : 'DOMAINE',
        'out' : 'ID',
        'func' : get_domaine_id #lambda text : text.split(' ')[0].replace('-','.')
    },
    {
        'in' : 'NIVEAU', # non utilisé
        'out' : 'Format de description',
        'func' : lambda x : FORMAT_DE_DESCRIPTION
    },
]

def get_theme_id_parent(text):
    text = re.sub('|'.join(CXY_SEPs), '.', text)
    return text.split(' ')[0].split('.')[0]

def get_theme_id(text):
    text = re.sub('|'.join(CXY_SEPs), '.', text)
    return text.split(' ')[0]


NEW_THEME = [
    {
        'in' : 'THEME',
        'out' : 'ID de la compétence parente',
        'func': get_theme_id_parent #lambda x : x.split(' ')[0].replace('-','.').split('.')[0]
    },
    {
        'in' : 'THEME',
        'out' : 'ID',
        'func': get_theme_id #lambda x : x.split(' ')[0].replace('-','.')
    },
    {
        'in' : 'THEME',
        'out' : 'Nom abrégé',
        'func': lambda x : x
    },
    {
        'in' : 'NIVEAU', # non utilisé
        'out' : 'Format de description',
        'func' : lambda x : FORMAT_DE_DESCRIPTION
    },
]


CATEGORY_CHANGE = [
    {
        'conversions' : NEW_NIVEAU,
        'prev_val' : None,
        'check' : 'NIVEAU'
    },
    {
        'conversions' : NEW_DOMAINE,
        'prev_val' : None,
        'check' : 'DOMAINE'
    },
    {
        'conversions' : NEW_THEME,
        'prev_val' : None,
        'check' : 'THEME'
    }
]


###################################################################
################ Définition de conversions ########################
###################################################################

# défintions des fonctions
def get_ID(text):
    text = re.sub('|'.join(CXY_SEPs), '.', text)
    return re.search("C\d+(\.\d+)*", text)[0].replace(".", "")

def get_parent_ID(text):
    text = re.sub('|'.join(CXY_SEPs), '.', text)
    return re.search("C\d+(\.\d+)*", text)[0]#.rpartition(".")[0]#.replace(".", "")

def multi_colomn_conv(*texts): # fonction avec plusieurs colonnes en entrée
    """
    Exemple de fonction avec plusieurs entrées,
    On ecrira
        def func(*texts)  (la variable  texts contiendra alors une liste de champs)
    ou
        def func(text1, text2, text3, ...) (chaque variable contiendra un champ)
    le nombre de variable dépendant du nombre de colonnes définies en entrée
    """
    extract_1 = texts [0] # text contient la liste
    extract_2 = texts [1] # some transformation on second line

    return 'multi_colonne : ' + extract_1 + ' ---- ' + extract_2

def parse_comment(text):

    lines = text.split('\n')
    return ''.join([f'<p>{line}</p>' if len(line) != 0 else '<br>' for line in lines])

# définition des conversions
CONVERSIONS = [
    {
        "in": "THEME",
        "out": "ID de la compétence parente",
        "func": get_parent_ID
    },
    {
        "in": "ITEM",  # colonne  où se trouvent les infos
        "out": "ID",  # colonne à remplir
        "func": get_ID  # la fonction pour extraire le texte souhaité
    },
    {
        "in": "ITEM",
        "out": "Nom abrégé",
        "func": lambda text: text  #  lambda = fct jetable
    },
    {
        "in": "COMMENTAIRE",
        "out": "Description",
        "func": parse_comment
    },
    {
        'in' : 'NIVEAU', # non utilisé
        'out' : 'Format de description',
        'func' : lambda x : FORMAT_DE_DESCRIPTION
    },
]
#    {
#        "in": ["COMMENTAIRE", "ITEM"],
#        "out": "Format de description",
#        "func": multi_colomn_conv
#    }
#]
