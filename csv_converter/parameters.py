#!/usr/bin/env python3

IN_CSV_DELIMITER = ','
IN_CSV_QUOTECHAR = '"'
OUT_CSV_DELIMITER = ','
OUT_CSV_QUOTECHAR = '"'

DEFAULT_HEADERS = ['ID de la compétence parente','ID', 'Nom abrégé', 'Description',	'Format de description', 'Valeurs du barème', 'configuration du barème', 'Type de règle de compétence (optionnelle)', "Règle d'objectif (optionnelle)", 'Règle de paramétrage (optionnelle)', 'IDs des compétences croisées associées', "ID d'exportation (optionnel)", 'Est un référentiel', 'Taxonomie']

DEFAULT_OUT_FILE = "converted.csv"

VALEURS_DU_BAREME = "1complètement raté,2plutôt raté,3plutôt réussi,4complètement réussi"

FORMAT_DE_DESCRIPTION = '1'

CONFIGURATION_DU_BAREME = """[{"scaleid":"3"},{"id":1,"scaledefault":0,"proficient":1},{"id":2,"scaledefault":0,"proficient":1},{"id":3,"scaledefault":0,"proficient":1},{"id":4,"scaledefault":1,"proficient":1}]"""

TAXONOMIE = "concept,competency,competency,competency"

# liste des sépateurs des références Cx.y.z autres que le .
CXY_SEPs = [',','-']
