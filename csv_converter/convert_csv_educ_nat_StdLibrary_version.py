#!/usr/bin/env python3

"""
Convertisseur fichers csv format Moodle vers SaCoche
"""

__author__ = "Daguhh"
__license__ = "unlicense.org"

import csv
import argparse

### Import des conversions définies par l'utilisateur
from .conversions_definitions import CONVERSIONS, CATEGORY_CHANGE #NEW_NIVEAU, NEW_DOMAINE

### Paramètres des fichiers
from .parameters import (
    DEFAULT_HEADERS,
    DEFAULT_OUT_FILE,
    IN_CSV_DELIMITER,
    IN_CSV_QUOTECHAR,
    OUT_CSV_DELIMITER,
    OUT_CSV_QUOTECHAR
)


def convert_csv(file_in, file_wanted=None, file_out=DEFAULT_OUT_FILE):
    """
    Converti les colonnes d'un csv (file_in) vers un autre csv (file_out)
    selon le format d'entetes d'un autre csv (file_wanted)
    Convertit les lignes une par une et en introduit une supplémentaire
    à chaque changement de catégorie.

    Args:
        file_in (str) : csv à convertir
        file_wanted (str) : csv avec les entêtes souhaitées (optionel)
        file_out : csv converti à inscrire (optionel)

    Les définitions des convertions sont inscrites dans le fichier
    "conversions_definitions.py."
    Les valeurs par défauts des paramètres optionels peuvent être défini
    dans le fichier "parameters.py"
    """

    with open(file_in) as csv_in, open('temp.csv', "w") as csv_out:
        reader = csv.DictReader(
            csv_in,
            delimiter=IN_CSV_DELIMITER,
            quotechar=IN_CSV_QUOTECHAR
        )
        writer = csv.DictWriter(
            csv_out,
            fieldnames=get_out_headers(file_wanted),
            delimiter=OUT_CSV_DELIMITER,
            quotechar=OUT_CSV_QUOTECHAR,
            quoting=csv.QUOTE_ALL,
            doublequote=True
        )

        writer.writeheader() # inscription des entêtes

        for line in reader: # On parcourt les lignes une à une
            new_line = {} # on créé une nouvelle ligne vierge à inscrire

            if all([len(l)==0 for l in line.values()]): # si la ligne lue est vide
                continue # on passe à la suivante

            # changement de thème, de domaine...
            for new_cat in CATEGORY_CHANGE: # Pour chaque catégorie
                new_val = line[new_cat['check']] # on regarde la colonne surveillée
                prev_val = new_cat['prev_val'] # et la compare à la valeur précédente

                if new_val != prev_val: # si la catérgorie a changé,
                    for conversion in new_cat['conversions']: # on parcourt les conversions
                        cols_in, col_out, transform = [conversion[k] for k in ["in", "out", "func"]] # on en récupère les paramètres

                        # Pour de multiples colonnes en entrée
                        cols_in = [cols_in] if isinstance(cols_in, str) else cols_in # créé un liste même si il n'y a qu'un nom de colonne en entrée
                        in_texts = [line[col_in] for col_in in cols_in] # recupère la liste des champs des colonnes
                        new_line[col_out] = transform(*in_texts) # et on execute la transformation

                        # pour une seule colonne d'entrée
                        #new_line[col_out] = transform(line[col_in])

                    writer.writerow(new_line)  # puis on incrit la ligne dans le fichier

                    new_line={} # enfin, on crée une nouvelle ligne vierge
                    new_cat['prev_val'] = new_val # et on sauvegarde de la catégorie actuelle

            # conversion des compétences
            for conversion in CONVERSIONS: # Pour chaque conversion
                cols_in, col_out, transform = [conversion[k] for k in ["in", "out", "func"]] # on récupère les paramètres

                try:
                    # Pour de multiples colonnes en entrée
                    cols_in = [cols_in] if isinstance(cols_in, str) else cols_in # créé un liste même si il n'y a qu'un nom de colonne en entrée
                    in_texts = [line[col_in] for col_in in cols_in] # recupère la liste des champs des colonnes
                    new_line[col_out] = transform(*in_texts) # et on execute la transformation

                    # pour une seule colonne d'entrée
                    #new_line[col_out] = transform(line[col_in]) # coeur du programme : execution de la transformation
                except TypeError as err: # si le contenu n'est pas du texte
                    typeerror_msg(err, col_in, col_out, line) # => message d'erreur
                except KeyError as err: # si le nom de colonne n'existe pas
                    keyerror_msg(col_in, col_out, InLine.headers, OutLine.headers) # => message d'erreur
                    raise KeyError # On arrete l'execution

            writer.writerow(new_line)  # inscription dans le fichier

    # Peut-être que ce dernier traitement n'est pas necessaire, on retire simplement les guillements superflus
    import re
    p0 = re.compile(r'^"([a-zA-Z0-9]+)"') # retirer les quillements pour texte sans espace debut ligne
    p1 = re.compile(r'(?<=[^"])"([a-zA-Z0-9]+)"') # idem quand quillement non doublé
    p2 = re.compile(r',""(?![a-zA-Z])') # retirer guillements pour champ vide
    p3 = re.compile(r'^("")') # idem début de ligne
    p4 = re.compile(r'("")$') # idem fin de ligne
    with open('temp.csv', 'r') as out_csv, open(file_out, 'w') as exit_csv :
        for line in out_csv:
            new_line = p0.sub('\g<1>', line)
            new_line = p1.sub('\g<1>', new_line)
            new_line = p2.sub(',', new_line)
            new_line = p3.sub('', new_line)
            new_line = p4.sub('', new_line)
            exit_csv.write(new_line)


def keyerror_msg(cols_in, col_out, headers_in, headers_out):
    """ message d'erreur en cas de mauvais nom de colonne """

    print("***********************************************")
    for col in cols_in:
        if not col in headers_in:
            print(f"Oups : le nom de colonne d'entrée '{col}' n'existe pas")
    if not col_out in headers_out:
        print(f"Oups : le nom de colonne de sortie '{col_out}' n'existe pas")
    print("programme stoppé...")
    #print("***********************************************\n")

def typeerror_msg(err, cols_in, col_out, line):
    """ message d'erreur en cas de ligne vide """

    print("***********************************************")
    print(
        f'Oups : problème de conversion de "{cols_in}" vers "{col_out}"',
        "\nle champ est-il vide?",
        "\nligne concernée: \n",
        ", ".join(line),
        "\nerreur:\n",
        err,
        "\nconversion ignorée...",
    )
    # raise err
    print("***********************************************\n")


def get_csv_headers(file):
    """ Retourne les entêtes d'un csv """

    with open(file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=",", quotechar='"')
        return spamreader.__next__()  # 1st line


def get_out_headers(file_wanted):
    """
    Retourne les entêtes d'un csv, si il n'est pas défini,
    retourne les valeurs par défaut
    """

    if file_wanted == None:  # si non définie par l'utilisateur
        return DEFAULT_HEADERS
    else:
        return get_csv_headers(file_wanted)


def get_arguments():
    """ Capture les arguements passés en ligne de commande """

    parser = argparse.ArgumentParser(
        description="""Convertisseur csv format Moodle vers format SacoCoche
        (Version bibliothèque standard)"""
    )

    parser.add_argument(
        "-i", "--input", help="csv à convertir", dest="file_in", required=True
    )

    parser.add_argument(
        "-w",
        "--wanted",
        help="csv avec les entêtes souhaitées (optionel)",
        dest="file_wanted",
    )

    parser.add_argument(
        "-o",
        "--ouput",
        help="csv converti à inscrire (optionel)",
        dest="file_out",
        default=DEFAULT_OUT_FILE,
    )

    args = parser.parse_args()

    return args


if __name__ == "__main__":

    args = get_arguments()
    convert_csv(
        file_in=args.file_in, file_wanted=args.file_wanted, file_out=args.file_out
    )

    print(f"fichier {args.file_out} créé")
